from django.db import models


class Job(models.Model):
    root = models.TextField()
    depth = models.PositiveIntegerField(null=True, blank=True)
    status = models.PositiveSmallIntegerField(
        choices=(
            (1, "Not started"),
            (2, "In progress"),
            (3, "Finished"),
            (4, "Failed")
        )
    )


class Result(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    value = models.TextField()
